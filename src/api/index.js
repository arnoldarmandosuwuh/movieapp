export const baseUrl = `https://api.themoviedb.org/3`
export const apiKey = `e852848561694f1300e3eac1deb19c49`
export const movieApi = `${baseUrl}/discover/movie?api_key=${apiKey}`
export const tvShowApi = `${baseUrl}/discover/tv?api_key=${apiKey}`
export const imageUrl = `https://image.tmdb.org/t/p/original`