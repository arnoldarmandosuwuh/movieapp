import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Moment from 'moment'
import { imageUrl } from '../api'

const MovieComponent = ({ movie }) => {
  return (
    <View style={styles.container}>
      <Image source={{ uri: `${imageUrl}${movie.poster_path}` }} style={styles.poster} />
      <View style={styles.content}>
        <Text style={styles.title} numberOfLines={4}>
          {movie.title}
        </Text>
        <View style={styles.date}>
          <MaterialCommunityIcons name="calendar" color="black" size={20} />
          <Text style={styles.textStyle}>{Moment(movie.release_date).format('D MMM yyyy')}</Text>
        </View>
        <View style={styles.date}>
          <FontAwesome5 name="users" color="black" size={20} />
          <Text style={styles.textStyle}>{movie.popularity}</Text>
        </View>
        <View style={styles.rating}>
          <MaterialCommunityIcons name="star" color="black" size={20} />
          <Text style={styles.textStyle}>{movie.vote_average}</Text>
        </View>
        <View style={styles.rating}>
          <MaterialCommunityIcons name="eye" color="black" size={20} />
          <Text style={styles.textStyle}>{movie.overview}</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 5,
    margin: 5,
  },
  poster: {
    height: 300,
    width: 200,
    borderRadius: 10,
    marginHorizontal: 5,
    alignSelf: 'center',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
  },
  date: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  cost: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  rating: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  detail: {
    flexDirection: 'row',
    marginBottom: 5,
    marginTop: 10,
  },
  textStyle: {
    color: '#000000',
    marginHorizontal: 5,
  },
})

export default MovieComponent
