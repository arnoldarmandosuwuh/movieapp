import React, { useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import auth from '@react-native-firebase/auth'
import AsyncStorage from '@react-native-community/async-storage'
import SplashScreen from '../screens/SplashScreen'
import IntroScreen from '../screens/IntroScreen'
import LoginScreen from '../screens/LoginScreen'
import RegisterScreen from '../screens/RegisterScreen'
import HomeScreen from '../screens/HomeScreen'
import ProfileScreen from '../screens/ProfileScreen'
import MapScreen from '../screens/MapScreen'
import ChatScreen from '../screens/ChatScreen'
import TvShowScreen from '../screens/TvShowScreen'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const AppNavigation = () => {
  const [firstLaunch, setFirstLaunch] = useState(null)
  const [initializing, setInitializing] = useState(true)
  const [user, setUser] = useState()

  const onAuthStateChanged = (user) => {
    setUser(user)
    if (initializing) setInitializing(false)
  }
  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  useEffect(() => {
    AsyncStorage.getItem('alreadyLaunched').then((value) => {
      if (value == null) {
        AsyncStorage.setItem('alreadyLaunched', 'true')
        setFirstLaunch(true)
      } else {
        setFirstLaunch(false)
      }
    })

    const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
    return subscriber
  }, [])

  if (initializing || firstLaunch === null) {
    return <SplashScreen />
  }

  const HomeNavigation = () => (
    <Stack.Navigator>
      <Stack.Screen name="Movie" component={TabNavigation} options={{ headerShown: false }} />
    </Stack.Navigator>
  )

  const TabNavigation = () => {
    return (
      <Tabs.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName

            if (route.name === 'Movie') {
              iconName = focused ? 'movie' : 'movie-outline'
            } else if (route.name === 'TvShow') {
              iconName = focused ? 'movie-open' : 'movie-open-outline'
            } else if (route.name === 'Chat') {
              iconName = focused ? 'chat' : 'chat-outline'
            } else if (route.name === 'Profile') {
              iconName = focused ? 'account' : 'account-outline'
            }
            return <MaterialCommunityIcons name={iconName} size={size} color={color} />
          },
        })}
        tabBarOptions={{
          style: {
            backgroundColor: '#fff',
          },
          labelStyle: {
            fontSize: 12,
            marginBottom: 5,
            fontWeight: 'bold',
          },
        }}>
        <Tabs.Screen name="Movie" component={HomeScreen} />
        <Tabs.Screen name="TvShow" component={TvShowScreen} />
        <Tabs.Screen name="Chat" component={ChatScreen} />
        <Tabs.Screen name="Profile" component={ProfileNavigation} />
      </Tabs.Navigator>
    )
  }

  const ProfileNavigation = () => (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Map" component={MapScreen} options={{ headerShown: true, title: 'Your Location' }} />
    </Stack.Navigator>
  )

  const MainNavigation = () =>
    firstLaunch ? (
      <Stack.Navigator>
        <Stack.Screen name="Intro" component={IntroScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }} />
      </Stack.Navigator>
    ) : (
        <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      )

  return (
    <NavigationContainer>
      {!user ? <MainNavigation /> : <HomeNavigation />}
    </NavigationContainer>
  )
}

export default AppNavigation
