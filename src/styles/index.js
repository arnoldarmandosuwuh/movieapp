import React from 'react'
import { StyleSheet, Dimensions } from 'react-native'

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

export const stylesIntro = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  slide: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  title: {
    color: '#191970',
    fontWeight: 'bold',
    fontSize: 20,
  },
  image: {
    width: 300,
    height: 300,
    marginTop: 15,
  },
  text: {
    marginTop: 15,
    color: '#000',
    textAlign: 'center',
    fontSize: 16,
  },
  buttonCircle: {
    width: 50,
    height: 50,
    backgroundColor: '#191970',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 200 / 2,
  },
})

export const stylesLogin = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logoContainer: {
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 300,
    height: 300,
  },
  formContainer: {
    flex: 1,
    padding: 20,
    width: '100%',
  },
  inputContainer: {
    marginBottom: 10,
  },
  input: {
    borderBottomColor: '#000',
    borderBottomWidth: 1,
  },
  formDecoration: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  formDecoration2: {
    flex: 1,
    height: 1,
    backgroundColor: '#000',
  },
  formDecorationText: {
    width: 50,
    textAlign: 'center',
  },
  buttonLogin: {
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 10,
  },
  buttonLoginFingerprint: {
    backgroundColor: '#191970',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 10,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 18,
    textTransform: 'uppercase',
  },
  buttonLoginGoogle: {
    backgroundColor: '#fc0303',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 5,
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 24,
    marginBottom: 24,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
})

export const stylesProfile = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  imageContainer: {
    width: width,
    height: height * 0.3,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
  },
  imgProfile: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
  },
  changePicture: {
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnChangePicture: {
    color: '#fff',
    fontSize: 18,
  },
  cardContainer: {
    width: width * 0.8,
    marginHorizontal: 30,
    marginTop: -40,
    elevation: 10,
    borderRadius: 10,
    padding: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 8,
    backgroundColor: '#fff',
  },
  contentContainer: {
    marginVertical: 10,
    flexDirection: 'row',
  },
  buttonContainer: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
  contentTitle: {
    flex: 0.2,
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'flex-start',
    textAlign: 'left',
  },
  contentText: {
    flex: 0.8,
    marginTop: 10,
    marginHorizontal: 10,
    alignItems: 'flex-end',
    textAlign: 'right',
  },
  buttonLogout: {
    backgroundColor: '#FC0303',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 5,
  },
  buttonLocation: {
    backgroundColor: '#3EC6FF',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 5,
  },
  buttonUpload: {
    backgroundColor: '#191970',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderRadius: 5,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    textTransform: 'uppercase',
  },
  btnFlipContainer: {
    margin: 20,
  },
  btnFlip: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  round: {
    width: '50%',
    height: '45%',
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  rectangle: {
    marginTop: 60,
    width: '50%',
    height: '20%',
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  btnTakeContainer: {
    alignItems: 'center',
  },
  btnTake: {
    marginTop: 20,
    height: 80,
    width: 80,
    borderRadius: 50,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
})
