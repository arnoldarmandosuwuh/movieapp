import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'

MapboxGL.setAccessToken(`pk.eyJ1IjoiYXJub2xkYXJtYW5kbzA3IiwiYSI6ImNqbWV5ZW5ueTFvMjAzcWxodWk5aW1pNWkifQ.vpy9jJBkKHo_CJpC9XXcYw`)

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]

const MapScreen = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch (error) {
                console.log("getLocation -> error", error)
            }
        }
        getLocation()
    }, [])

    const MapBoxAnnotation = coordinates.map((value, index) => (
        <MapboxGL.PointAnnotation
            id={index.toString()}
            key={index.toString()}
            coordinate={value}>
            <MapboxGL.Callout title={`Longitude: ${value[0]}\nLatitude: ${value[1]}`} />
        </MapboxGL.PointAnnotation>
    ))

    return (
        <View style={styles.container}>
            <MapboxGL.MapView style={{ flex: 1 }} attributionEnabled={true}>
                <MapboxGL.UserLocation visible={true} />
                <MapboxGL.Camera followUserLocation={true} />
                {MapBoxAnnotation}
            </MapboxGL.MapView>
        </View>
    )
}

export default MapScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
})
