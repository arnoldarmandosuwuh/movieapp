import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, StatusBar, FlatList, ActivityIndicator } from 'react-native'
import Axios from 'axios'
import { tvShowApi } from '../api'
import TvShowItem from '../components/TvShowComponent'

const TvShowScreen = () => {
    const [tvShow, setTvShow] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        loadTvShow()
    }, [])

    const loadTvShow = () => {
        setIsLoading(true)
        Axios.get(tvShowApi)
            .then((res) => {
                setTvShow(res.data.results)
                setIsLoading(false)
                console.log(tvShow)
            })
            .catch((error) => {
                setIsLoading(false)
                alert(error)
            })
    }

    if (isLoading) return <ActivityIndicator size="large" color='#3EC6FF' style={styles.loading} />

    return (
        <View style={styles.container}>
            <FlatList
                data={tvShow}
                keyExtractor={(data) => data.id.toString()}
                renderItem={(item) => <TvShowItem tvShow={item.item} />}
                ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
})

export default TvShowScreen