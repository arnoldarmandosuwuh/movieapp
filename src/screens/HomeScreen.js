import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, StatusBar, FlatList, ActivityIndicator } from 'react-native'
import Axios from 'axios'
import { movieApi } from '../api'
import MovieItem from '../components/MovieComponent'

const HomeScreen = () => {
  const [data, setData] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    loadMovie()
  }, [])

  const loadMovie = () => {
    setIsLoading(true)
    Axios.get(movieApi)
      .then((res) => {
        setData(res.data.results)
        setIsLoading(false)
        console.log(data)
      })
      .catch((error) => {
        setIsLoading(false)
        alert(error)
      })
  }

  if (isLoading) return <ActivityIndicator size="large" color="#3EC6FF" style={styles.loading} />

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={(data) => data.id.toString()}
        renderItem={(item) => <MovieItem movie={item.item} />}
        ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
})

export default HomeScreen
