import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import auth from '@react-native-firebase/auth'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import { stylesLogin as styles } from '../styles'

const RegisterScreen = ({ navigation }) => {
  const [email, setEmail] = useState(null)
  const [password, setPassword] = useState(null)
  const [rePassword, setRePassword] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  const registerHandler = () => {
    if (email && password && rePassword) {
      if (password === rePassword) {
        registerProcess()
      } else {
        alert(`Password doesn't match!`)
      }
    } else {
      alert(`Please fill out the form correctly`)
    }
  }

  const registerProcess = () => {
    setIsLoading(true)

    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        setIsLoading(false)
        navigation.navigate('Login')
      })
      .catch((error) => {
        setIsLoading(false)
        alert(error)
      })
  }

  if (isLoading) {
    return <ActivityIndicator size="large" color="#3EC6FF" style={styles.loading} />
  }

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require('../assets/images/logo.png')} style={styles.logo} />
      </View>
      <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
          <Text>Email</Text>
          <TextInput
            value={email}
            onChangeText={(email) => setEmail(email)}
            style={styles.input}
            placeholder="Email"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>Password</Text>
          <TextInput
            secureTextEntry={true}
            value={password}
            onChangeText={(password) => setPassword(password)}
            style={styles.input}
            placeholder="Password"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>Re-Password</Text>
          <TextInput
            secureTextEntry={true}
            value={rePassword}
            onChangeText={(rePassword) => setRePassword(rePassword)}
            style={styles.input}
            placeholder="Re-Password"
          />
        </View>
        <View style={styles.inputContainer}>
          <TouchableOpacity style={styles.buttonLogin} onPress={() => registerHandler()}>
            <MaterialCommunityIcons name="check-bold" size={20} color="#fff">
              <Text style={styles.buttonText}>Register</Text>
            </MaterialCommunityIcons>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default RegisterScreen
