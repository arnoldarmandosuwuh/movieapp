import React, { useState, useEffect } from 'react'
import { View, Text, StatusBar, Modal, Dimensions, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import AsyncStorage from '@react-native-community/async-storage'
import { GoogleSignin } from '@react-native-community/google-signin'
import { RNCamera } from 'react-native-camera'
import storage from '@react-native-firebase/storage'
import auth from '@react-native-firebase/auth'
import { stylesProfile as styles } from '../styles'

const ProfileScreen = ({ navigation }) => {
  const [userInfo, setUserInfo] = useState(null)
  const [isVisible, setIsVisible] = useState(false)
  const [type, setType] = useState('back')
  const [photo, setPhoto] = useState(null)
  const [userToken, setUserToken] = useState(null)
  const [userEmail, setUserEmail] = useState(null)
  const [loginType, setLoginType] = useState(null)

  const googleLogin = 'Google'
  const emailLogin = 'Email'

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token')
        const type = await AsyncStorage.getItem('type')
        setUserToken(token)
        setLoginType(type)
      } catch (error) {
        console.log(error)
      }
    }
    getToken()
    getCurrentUser()
  }, [userInfo])

  const getCurrentUser = async () => {
    const user = auth().currentUser
    if (user != null) setUserInfo(user)
  }

  const logoutHandler = async () => {
    try {
      if (loginType === googleLogin) {
        await GoogleSignin.revokeAccess()
        await GoogleSignin.signOut()
        await AsyncStorage.removeItem('token')
        await AsyncStorage.removeItem('type')
      } else {
        await AsyncStorage.removeItem('token')
        await AsyncStorage.removeItem('type')
        await auth().signOut()
      }
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }]
      })
    } catch (error) {
      console.log(error)
    }
  }

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back')
  }
  const takePicture = async () => {
    const options = { quality: 0.5, base64: true }
    if (camera) {
      const data = await camera.takePictureAsync(options)
      console.log('takePicture -> data', data)
      setPhoto(data)
      setIsVisible(false)
    }
  }

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime()
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert(`Upload Success`)
        navigation.navigate('Profile')
      })
      .catch((error) => {
        alert(error)
      })
  }

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{ flex: 1 }}>
          <RNCamera
            style={{ flex: 1 }}
            ref={ref => {
              camera = ref
            }}
            type={type}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity style={styles.btnFlip} onPress={() => toogleCamera()}>
                <Icon name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                <Feather name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={photo === null ? require('../assets/images/default_photo.jpg') : { uri: photo.uri }}
          style={styles.imgProfile}
        />
        <TouchableOpacity
          style={styles.changePicture}
          onPress={() => {
            setIsVisible(true)
          }}>
          <Text style={styles.btnChangePicture}>Change Picture</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.cardContainer}>
        <View style={styles.contentContainer}>
          <Text style={styles.contentTitle}>Email</Text>
          <Text style={styles.contentText}>{userInfo && userInfo.email}</Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonUpload} onPress={() => uploadImage(photo.uri)}>
            <Icon name="upload" size={25} color="#fff">
              <Text style={styles.buttonText}>Upload</Text>
            </Icon>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonLocation} onPress={() => navigation.navigate('Map')}>
            <Icon name="map-marker-radius" size={25} color="#fff">
              <Text style={styles.buttonText}>Location</Text>
            </Icon>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonLogout} onPress={() => logoutHandler()}>
            <Icon name="logout" size={25} color="#fff">
              <Text style={styles.buttonText}>Logout</Text>
            </Icon>
          </TouchableOpacity>
        </View>
      </View>
      {renderCamera()}
    </View>
  )
}

export default ProfileScreen
