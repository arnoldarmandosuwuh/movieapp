import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'

const ChatScreen = ({ navigation }) => {
    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const user = auth().currentUser
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messagesMovie')
            if (db) {
                db.off()
            }
        }
    }, [])

    const getData = () => {
        database().ref('messagesMovie').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }

    const onSend = ((messages = []) => {
        for (let i = 0; i < messages.length; i++) {
            database().ref('messagesMovie').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    })
    return (
        <GiftedChat
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: 'https://img.icons8.com/doodle/48/000000/user-male--v1.png'
            }}
        />
    )
}

export default ChatScreen

const styles = StyleSheet.create({})
