import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  Image,
  TextInput,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import auth from '@react-native-firebase/auth'
import { CommonActions } from '@react-navigation/native'
import AsyncStorage from '@react-native-community/async-storage'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import { stylesLogin as styles } from '../styles'

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const googleLogin = 'Google'
  const emailLogin = 'Email'

  const saveToken = async (token, type) => {
    try {
      await AsyncStorage.setItem('token', token)
      await AsyncStorage.setItem('type', type)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    navigation.dispatch((state) => {
      const routes = state.routes.filter((route) => route.name !== 'Intro')

      return CommonActions.reset({
        ...state,
        routes,
        index: routes.length - 1,
      })
    })
    configureGoogleSignIn()
  }, [])

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId: '416425879758-ph57clcbj5qqppf4b14bfvj18t1fio9r.apps.googleusercontent.com',
    })
  }

  const signInWithGoogle = async () => {
    setIsLoading(true)

    try {
      const { idToken } = await GoogleSignin.signIn()
      const credential = auth.GoogleAuthProvider.credential(idToken)
      auth().signInWithCredential(credential)
      saveToken(idToken, googleLogin)
      setIsLoading(false)
      navigation.reset({
        index: 0,
        routes: [{ name: 'Movie' }]
      })
    } catch (error) {
      setIsLoading(false)
      console.log('SignInWithGoogle -> error', error)
    }
  }

  const loginHandler = () => {
    if (!email || !password) alert(`Please fill out the form correctly`)
    else {
      setIsLoading(true)
      return auth()
        .signInWithEmailAndPassword(email, password)
        .then((res) => {
          saveToken(res.user.uid, emailLogin)
          navigation.reset({
            index: 0,
            routes: [{ name: 'Movie' }]
          })
        })
        .catch((error) => {
          setIsLoading(false)
          alert(error)
          console.log('Error -> ', error)
        })
    }
  }

  if (isLoading) {
    return <ActivityIndicator size="large" color="#3EC6FF" style={styles.loading} />
  }

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require('../assets/images/logo.png')} style={styles.logo} />
      </View>
      <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
          <Text>Email</Text>
          <TextInput
            value={email}
            onChangeText={(email) => setEmail(email)}
            style={styles.input}
            placeholder="Email"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text>Password</Text>
          <TextInput
            secureTextEntry={true}
            value={password}
            onChangeText={(password) => setPassword(password)}
            style={styles.input}
            placeholder="Password"
          />
        </View>
        <View style={styles.inputContainer}>
          <TouchableOpacity style={styles.buttonLogin} onPress={() => loginHandler()}>
            <MaterialCommunityIcons name="login" size={20} color="#fff">
              <Text style={styles.buttonText}>Login</Text>
            </MaterialCommunityIcons>
          </TouchableOpacity>
        </View>
        <View style={styles.formDecoration}>
          <View style={styles.formDecoration2} />
          <View>
            <Text style={styles.formDecorationText}>OR</Text>
          </View>
          <View style={styles.formDecoration2} />
        </View>
        <View style={styles.inputContainer}>
          <GoogleSigninButton
            onPress={() => signInWithGoogle()}
            style={{ width: '100%', height: 40 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
          />
        </View>
        <View style={styles.footer}>
          <Text>Belum mempunyai akun ? </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={{ color: '#3EC6FF' }}>Buat Akun</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default LoginScreen
