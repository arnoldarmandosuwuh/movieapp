/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react'
import firebase from '@react-native-firebase/app'
import Navigation from './src/navigations'
import OneSignal from 'react-native-onesignal'
import codePush from 'react-native-code-push'
import { Alert } from 'react-native'

// code-push release-react MovieApp android
// code-push deployment list MovieApp
// code-push promote MovieApp Staging Production

var firebaseConfig = {
  apiKey: 'AIzaSyBLVP4dUrf52332vzY6xl4CI8mhm9Sy2Oc',
  authDomain: 'sanbercode-89b39.firebaseapp.com',
  databaseURL: 'https://sanbercode-89b39.firebaseio.com',
  projectId: 'sanbercode-89b39',
  storageBucket: 'sanbercode-89b39.appspot.com',
  messagingSenderId: '416425879758',
  appId: '1:416425879758:web:c26547ec4b0f8fbd09889f',
  measurementId: 'G-3XKBTQZM7K',
}
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("7fd90285-10e0-4faf-a25b-56782cb6433d", {
      kOSSettingsKeyAutoPrompt : false, 
      kOSSettingsKeyInAppLaunchURL: false, 
      kOSSettingsKeyInFocusDisplayOption:2
    })
    OneSignal.inFocusDisplaying(2)

    OneSignal.addEventListener("received", onReceived)
    OneSignal.addEventListener("opened", onOpened)
    OneSignal.addEventListener("ids", onIds)

    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
    }, SyncStatus);

    return () => {
      OneSignal.removeEventListener("received", onReceived)
      OneSignal.removeEventListener("opened", onOpened)
      OneSignal.removeEventListener("ids", onIds)
    }
}, [])

const SyncStatus = (status) => {
  switch (status) {
    case codePush.SyncStatus.CHECKING_FOR_UPDATE:
      console.log('Checking Update')
      break

    case codePush.SyncStatus.DOWNLOADING_PACKAGE:
      console.log('Downloading Package')
      break

    case codePush.SyncStatus.UP_TO_DATE:
      console.log('Up to date')
      break

    case codePush.SyncStatus.INSTALLING_UPDATE:
      console.log('Installing Update')
      break

    case codePush.SyncStatus.UPDATE_INSTALLED:
      Alert.alert('Notification', 'Update Installed')
      break

    case codePush.SyncStatus.AWAITING_USER_ACTION:
      console.log('Awaiting user action')
      break
    default:
      break
  }
}

  const onReceived = (notification) => {
    console.log("onReceived -> notification", notification)
  }

  const onOpened = (openResult) => {
    console.log("onOpened -> openResult", openResult)
  }

  const onIds = (device) => {
    console.log("onIds -> device", device)
  }

  return <Navigation />
}

export default App
